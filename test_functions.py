from functions import get_job_variables_names, spread, cartesian_product, join


def test_get_job_variables_names():
    input_value = {"variables": {
        "A": "1,2",
        "B": "3,4"
    }}
    output_value = ["A", "B"]
    assert get_job_variables_names(input_value) == output_value


def test_get_job_variables_names__no_variables_key():
    input_value = {}
    output_value = []
    assert get_job_variables_names(input_value) == output_value


def test_get_job_variables_names__empty_variables_key():
    input_value = {"variables": {}}
    output_value = []
    assert get_job_variables_names(input_value) == output_value


def test_spread():
    input_value = {
        "A": ["1", "2"],
        "B": ["3", "4"]
    }
    output_value = [
        [("A", "1"), ("A", "2")],
        [("B", "3"), ("B", "4")]
    ]
    assert spread(input_value) == output_value


def test_cartesian_product():
    input_value = [
        [("A", "1"), ("A", "2")],
        [("B", "3"), ("B", "4")]
    ]
    output_value = [
        (("A", "1"), ("B", "3")),
        (("A", "1"), ("B", "4")),
        (("A", "2"), ("B", "3")),
        (("A", "2"), ("B", "4"))
    ]
    assert cartesian_product(input_value) == output_value


def test_join():
    input_value = [
        (("A", "1"), ("B", "3")),
        (("A", "1"), ("B", "4")),
        (("A", "2"), ("B", "3")),
        (("A", "2"), ("B", "4"))
    ]
    output_value = [
        {"A": "1", "B": "3"},
        {"A": "1", "B": "4"},
        {"A": "2", "B": "3"},
        {"A": "2", "B": "4"}
    ]
    assert join(input_value) == output_value
