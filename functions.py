"""
Several utility functions
"""
from itertools import product


def get_job_variables_names(job_definition):
    """
    Extract variables names from a CI job definition
    >>> get_job_variables_names({'variables': {'A': '1,2', 'B': '3,4'}})
    ['A', 'B']

    :param job_definition: A GitLab CI job definition
    """

    if "variables" in job_definition.keys() and job_definition["variables"] is not None:
        return [k for k in job_definition["variables"].keys()]
    else:
        return []


def spread(dict_to_spread):
    """
    Spread variables
    >>> spread({'A': ['1', '2'], 'B': ['3', '4']})
    [[('A', '1'), ('A', '2')], [('B', '3'), ('B', '4')]]
    """
    return [[(k, v) for v in dict_to_spread[k]] for k in dict_to_spread.keys()]


def cartesian_product(variables):
    """
    Calculate Cartesian product of a list of lists
    >>> cartesian_product([[('A', '1'), ('A', '2')], [('B', '3'), ('B', '4')]])
    [(('A', '1'), ('B', '3')), (('A', '1'), ('B', '4')), (('A', '2'), ('B', '3')), (('A', '2'), ('B', '4'))]
    """
    return [v for v in product(*variables)]


def join(variables):
    """
    Transform a list of lists of tuples into a list of dicts
    >>> join([[('A', '1'), ('B', '3')], [('A', '1'), ('B', '4')], [('A', '2'), ('B', '3')], [('A', '2'), ('B', '4')]])
    [{'A': '1', 'B': '3'}, {'A': '1', 'B': '4'}, {'A': '2', 'B': '3'}, {'A': '2', 'B': '4'}]
    """
    return [dict(row) for row in variables]
